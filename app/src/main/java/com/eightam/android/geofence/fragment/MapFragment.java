package com.eightam.android.geofence.fragment;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.android.geofence.R;
import com.eightam.android.geofence.manager.GeofenceManager;
import com.eightam.android.geofence.model.Geofence;
import com.eightam.android.geofence.util.EventBusUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MapFragment extends SupportMapFragment
        implements GoogleMap.OnMyLocationChangeListener, GoogleMap.OnMapClickListener,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener {

    public static final int MODE_NORMAL = 0;
    public static final int MODE_GEOFENCE_ADD = 1;
    public static final int MODE_GEOFENCE_REMOVE = 2;

    private static final String FRAGMENT_GEOFENCE_EDITOR_DIALOG = "GeofenceEditorDialog";

    private static final float DEFAULT_RADIUS_IN_METERS = 40.0f;

    private GeofenceManager mGeofenceManager;

    private GoogleMap mMap;
    private int mMode;
    private boolean mMapHasBeenSetup;
    private boolean mMapHasBeenCentered;
    private BidiMap<Marker, Geofence> mGeofenceMarkers;
    private Map<Marker, Circle> mGeofenceCircles;

    public static MapFragment newInstance(GeofenceManager geofenceManager) {
        MapFragment fragment = new MapFragment();
        fragment.mGeofenceManager = geofenceManager;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        initialize();
        EventBusUtils.register(MapFragment.this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setupMapIfNecessary();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupMapIfNecessary();
        EventBusUtils.register(MapFragment.this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBusUtils.unregister(MapFragment.this);
    }

    @Override
    public void onMyLocationChange(Location location) {
        if (!mMapHasBeenCentered) {
            centerMapOnLocation(location);

            mMapHasBeenCentered = true;
            mMap.setOnMyLocationChangeListener(null);
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (mMode == MODE_GEOFENCE_ADD) {
            addGeofence(UUID.randomUUID().toString(), latLng);
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        // TODO Register / unregister geofence transition listener when it becomes visible / invisible.
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        switch (mMode) {
            case MODE_NORMAL: {
                Geofence geofence = mGeofenceMarkers.get(marker);

                if (geofence != null) {
                    showGeofenceEditorDialog(geofence);
                    return true;
                }
                break;
            }

            case MODE_GEOFENCE_REMOVE: {
                Geofence geofence = mGeofenceMarkers.get(marker);

                if (geofence != null) {
                    removeGeofence(geofence.id, marker);
                    return true;
                }
                break;
            }
        }
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        updateGeofenceCoordinateFromMarker(marker);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        updateGeofenceCoordinateFromMarker(marker);
    }

    public int getMode() {
        return mMode;
    }

    public void setMode(int mode) {
        switch (mode) {
            case MODE_NORMAL:
            case MODE_GEOFENCE_ADD:
            case MODE_GEOFENCE_REMOVE:
                mMode = mode;
                break;
        }
    }

    public void onEventMainThread(GeofenceEditorDialogFragment.OnGeofenceUpdatedEvent event) {
        updateGeofence(event.geofence);
    }

    private void initialize() {
        mGeofenceMarkers = new DualHashBidiMap<>();
        mGeofenceCircles = new HashMap<>();
    }

    private void setupMapIfNecessary() {
        mMap = getMap();

        if (mMap != null && !mMapHasBeenSetup) {
            UiSettings uiSettings = mMap.getUiSettings();
            uiSettings.setMyLocationButtonEnabled(false);
            uiSettings.setRotateGesturesEnabled(false);
            uiSettings.setZoomControlsEnabled(false);

            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationChangeListener(MapFragment.this);
            mMap.setOnMapClickListener(MapFragment.this);
            mMap.setOnCameraChangeListener(MapFragment.this);
            mMap.setOnMarkerClickListener(MapFragment.this);
            mMap.setOnMarkerDragListener(MapFragment.this);

            mMapHasBeenSetup = true;
        }
    }

    private void centerMapOnLocation(Location location) {
        if (mMap != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                    new LatLng(location.getLatitude(), location.getLongitude()), 18);

            mMap.animateCamera(cameraUpdate);
        }
    }

    private void addGeofence(String requestId, LatLng latLng) {
        Context context = getActivity();

        if (context != null && mMap != null) {
            Geofence geofence = new Geofence();
            geofence.id = requestId;
            geofence.title = context.getResources().getString(R.string.label_new_geofence);
            geofence.coordinate = latLng;
            geofence.radius = DEFAULT_RADIUS_IN_METERS;

            try {
                mGeofenceManager.addGeofence(geofence);

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.draggable(false);
                markerOptions.position(geofence.coordinate);
                markerOptions.title(geofence.title);

                CircleOptions circleOptions = new CircleOptions();
                circleOptions.center(geofence.coordinate);
                circleOptions.radius(geofence.radius);
                circleOptions.fillColor(context.getResources().getColor(R.color.geofence_area_fill_color));
                circleOptions.strokeWidth(0);

                Marker marker = mMap.addMarker(markerOptions);
                Circle circle = mMap.addCircle(circleOptions);

                mGeofenceMarkers.put(marker, geofence);
                mGeofenceCircles.put(marker, circle);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void removeGeofence(String requestId, Marker marker) {
        try {
            mGeofenceManager.removeGeofence(requestId);

            Circle circle = mGeofenceCircles.get(marker);

            if (circle != null) {
                circle.remove();
            }

            marker.remove();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateGeofence(Geofence geofence) {
        try {
            mGeofenceManager.addGeofence(geofence);

            Marker marker = mGeofenceMarkers.getKey(geofence);

            if (marker != null) {
                Circle circle = mGeofenceCircles.get(marker);

                if (circle != null) {
                    circle.setRadius(geofence.radius);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateGeofenceCoordinateFromMarker(Marker marker) {
        Geofence geofence = mGeofenceMarkers.get(marker);

        if (geofence != null) {
            geofence.coordinate = marker.getPosition();
        }
    }

    private void showGeofenceEditorDialog(Geofence geofence) {
        GeofenceEditorDialogFragment geofenceEditorDialogFragment =
                (GeofenceEditorDialogFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_GEOFENCE_EDITOR_DIALOG);

        if (geofenceEditorDialogFragment == null) {
            geofenceEditorDialogFragment = GeofenceEditorDialogFragment.newInstance(geofence);
            geofenceEditorDialogFragment.show(getChildFragmentManager(), FRAGMENT_GEOFENCE_EDITOR_DIALOG);
        }
    }

}
