package com.eightam.android.geofence;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.eightam.android.geofence.constant.K;
import com.eightam.android.geofence.fragment.MapFragment;
import com.eightam.android.geofence.manager.GeofenceManager;
import com.eightam.android.geofence.provider.GeofenceManagerProvider;
import com.eightam.android.geofence.util.FormatterUtils;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

public class MainActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    private static final String FRAGMENT_MAP = "Map";

    private Spinner mSpinnerMapMode;
    private AppInForegroundBroadcastReceiver mAppInForegroundBroadcastReceiver;

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String mapModeAsString = (String) mSpinnerMapMode.getItemAtPosition(position);

        if (getResources().getString(R.string.map_mode_normal).equals(mapModeAsString)) {
            setMapMode(MapFragment.MODE_NORMAL);

        } else if (getResources().getString(R.string.map_mode_geofence_add).equals(mapModeAsString)) {
            setMapMode(MapFragment.MODE_GEOFENCE_ADD);

        } else if (getResources().getString(R.string.map_mode_geofence_remove).equals(mapModeAsString)) {
            setMapMode(MapFragment.MODE_GEOFENCE_REMOVE);

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSpinnerMapMode = (Spinner) findViewById(R.id.spinner_map_mode);
        mSpinnerMapMode.setOnItemSelectedListener(MainActivity.this);

        if (savedInstanceState == null) {
            showMap(((GeofenceApplication) getApplication()).getGeofenceManager());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerAppInForegroundBroadcastReceiver((GeofenceApplication) getApplication());
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterAppInForegroundBroadcastReceiver();
    }

    private void showMap(GeofenceManager geofenceManager) {
        MapFragment mapFragment =
                (MapFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_MAP);

        if (mapFragment == null) {
            mapFragment = MapFragment.newInstance(geofenceManager);

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, mapFragment, FRAGMENT_MAP)
                    .commit();
        }
    }

    private void setMapMode(int mapMode) {
        MapFragment mapFragment =
                (MapFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_MAP);

        if (mapFragment != null) {
            mapFragment.setMode(mapMode);
        }
    }

    private void registerAppInForegroundBroadcastReceiver(GeofenceManagerProvider geofenceManagerProvider) {
        if (mAppInForegroundBroadcastReceiver == null) {
            mAppInForegroundBroadcastReceiver =
                    new AppInForegroundBroadcastReceiver(geofenceManagerProvider.getGeofenceManager());
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(K.intent.action.ACTION_FORWARD_NOTIFY_GEOFENCE_TRANSITION);
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);

        registerReceiver(mAppInForegroundBroadcastReceiver, filter);
    }

    private void unregisterAppInForegroundBroadcastReceiver() {
        if (mAppInForegroundBroadcastReceiver != null) {
            try {
                unregisterReceiver(mAppInForegroundBroadcastReceiver);
            } catch (Exception e) {
            }
        }
    }

    private class AppInForegroundBroadcastReceiver extends BroadcastReceiver {

        private GeofenceManager mGeofenceManager;

        public AppInForegroundBroadcastReceiver(GeofenceManager geofenceManager) {
            mGeofenceManager = geofenceManager;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (K.intent.action.ACTION_FORWARD_NOTIFY_GEOFENCE_TRANSITION.equals(action)) {
                abortBroadcast();

                GeofencingEvent event = GeofencingEvent.fromIntent(intent);

                if (event != null && !event.hasError()) {
                    showNotificationForGeofencingEvent(context, event);
                }
            }
        }

        private void showNotificationForGeofencingEvent(Context context, GeofencingEvent event) {
            List<Geofence> geofences = event.getTriggeringGeofences();
            int geofenceTransition = event.getGeofenceTransition();

            if (geofences != null) {
                for (Geofence geofence : geofences) {
                    com.eightam.android.geofence.model.Geofence geofenceData = mGeofenceManager.getGeofence(geofence.getRequestId());

                    if (geofenceData != null) {
                        showAlert(FormatterUtils.geofenceTransitionEvent(context, geofenceTransition, geofenceData));
                    }
                }
            }
        }

    }

}
