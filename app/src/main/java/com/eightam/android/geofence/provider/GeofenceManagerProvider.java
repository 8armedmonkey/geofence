package com.eightam.android.geofence.provider;

import com.eightam.android.geofence.manager.GeofenceManager;

public interface GeofenceManagerProvider {

    GeofenceManager getGeofenceManager();

}
