package com.eightam.android.geofence.constant;

public final class K {

    public static final class intent {

        public static final String BASE = "com.eightam.android.geofence.intent";

        public static final class action {

            public static final String BASE = intent.BASE + ".action";
            public static final String ACTION_NOTIFY_GEOFENCE_TRANSITION = BASE + ".NOTIFY_GEOFENCE_TRANSITION";
            public static final String ACTION_FORWARD_NOTIFY_GEOFENCE_TRANSITION = BASE + ".FORWARD_NOTIFY_GEOFENCE_TRANSITION";

        }

    }

    public static final class notification {

        public static final int NOTIFICATION_ID_GEOFENCE_EVENT = 1;

    }

}
