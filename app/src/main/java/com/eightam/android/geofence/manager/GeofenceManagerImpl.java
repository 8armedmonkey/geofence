package com.eightam.android.geofence.manager;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.eightam.android.geofence.constant.K;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeofenceManagerImpl implements GeofenceManager {

    private static final String TAG = GeofenceManagerImpl.class.getSimpleName();
    private static final int LOITERING_DELAY_MILLIS = 60000;

    private Context mAppContext;
    private GoogleApiClient mGoogleApiClient;
    private Map<String, com.eightam.android.geofence.model.Geofence> mGeofences;

    public GeofenceManagerImpl(Context context, GoogleApiClient googleApiClient) {
        mAppContext = context.getApplicationContext();
        mGoogleApiClient = googleApiClient;
        mGeofences = new HashMap<>();
    }

    public void addGeofence(com.eightam.android.geofence.model.Geofence geofence) throws Exception {
        assertGoogleApiClientConnected();

        GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                .addGeofence(new Geofence.Builder()
                        .setRequestId(geofence.id)
                        .setCircularRegion(geofence.coordinate.latitude, geofence.coordinate.longitude, geofence.radius)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
                                | Geofence.GEOFENCE_TRANSITION_DWELL
                                | Geofence.GEOFENCE_TRANSITION_EXIT)
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .setLoiteringDelay(LOITERING_DELAY_MILLIS)
                        .build())
                .build();

        LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, geofencingRequest, getGeofencePendingIntent())
                .setResultCallback(new ResultCallback<Status>() {

                    @Override
                    public void onResult(Status status) {
                        Log.d(TAG, "***** Adding geofences, status.success: " + status.isSuccess());
                    }

                });

        mGeofences.put(geofence.id, geofence);
    }

    public void removeGeofence(String requestId) throws Exception {
        assertGoogleApiClientConnected();

        LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, Arrays.asList(requestId))
                .setResultCallback(new ResultCallback<Status>() {

                    @Override
                    public void onResult(Status status) {
                        Log.d(TAG, "***** Removing geofences, status.success: " + status.isSuccess());
                    }

                });

        mGeofences.remove(requestId);
    }

    public void clearGeofences() throws Exception {
        assertGoogleApiClientConnected();

        LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, new ArrayList<>(mGeofences.keySet()))
                .setResultCallback(new ResultCallback<Status>() {

                    @Override
                    public void onResult(Status status) {
                        Log.d(TAG, "***** Removing geofences, status.success: " + status.isSuccess());
                    }

                });

        mGeofences.clear();
    }

    public List<String> getGeofenceRequestIds() {
        return new ArrayList<>(mGeofences.keySet());
    }

    public List<com.eightam.android.geofence.model.Geofence> getGeofences() {
        return new ArrayList<>(mGeofences.values());
    }

    public com.eightam.android.geofence.model.Geofence getGeofence(String requestId) {
        return mGeofences.get(requestId);
    }

    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(K.intent.action.ACTION_NOTIFY_GEOFENCE_TRANSITION);
        return PendingIntent.getBroadcast(mAppContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void assertGoogleApiClientConnected() throws Exception {
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            throw new Exception("Google API client is not connected.");
        }
    }

}
