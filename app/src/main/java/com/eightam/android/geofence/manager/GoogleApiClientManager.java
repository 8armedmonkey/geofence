package com.eightam.android.geofence.manager;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

import de.greenrobot.event.EventBus;

public class GoogleApiClientManager
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Context mAppContext;
    private GoogleApiClient mGoogleApiClient;

    public GoogleApiClientManager(Context context, Api... apis) {
        mAppContext = context.getApplicationContext();
        mGoogleApiClient = buildGoogleApiClientWithApis(apis);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        OnConnectedEvent event = new OnConnectedEvent();
        event.manager = GoogleApiClientManager.this;
        event.connectionHint = connectionHint;

        EventBus.getDefault().post(event);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        OnConnectionSuspendedEvent event = new OnConnectionSuspendedEvent();
        event.manager = GoogleApiClientManager.this;
        event.cause = cause;

        EventBus.getDefault().post(event);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        OnConnectionFailedEvent event = new OnConnectionFailedEvent();
        event.manager = GoogleApiClientManager.this;
        event.result = result;

        EventBus.getDefault().post(event);
    }

    public void connect() {
        mGoogleApiClient.connect();
    }

    public void disconnect() {
        mGoogleApiClient.disconnect();
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    private GoogleApiClient buildGoogleApiClientWithApis(Api... apis) {
        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(mAppContext)
                .addConnectionCallbacks(GoogleApiClientManager.this)
                .addOnConnectionFailedListener(GoogleApiClientManager.this);

        for (Api api : apis) {
            builder.addApi(api);
        }
        return builder.build();
    }

    public static class OnConnectedEvent {

        public GoogleApiClientManager manager;
        public Bundle connectionHint;

    }

    public static class OnConnectionSuspendedEvent {

        public GoogleApiClientManager manager;
        public int cause;

    }

    public static class OnConnectionFailedEvent {

        public GoogleApiClientManager manager;
        public ConnectionResult result;

    }

}
