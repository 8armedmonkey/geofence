package com.eightam.android.geofence;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.eightam.android.geofence.manager.GoogleApiClientManager;
import com.eightam.android.geofence.util.EventBusUtils;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

public class BaseActivity extends ActionBarActivity {

    public void onEventMainThread(GoogleApiClientManager.OnConnectionFailedEvent event) {
        showAlert(getResources().getString(R.string.err_google_api_client_connection_failed));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBusUtils.register(BaseActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBusUtils.unregister(BaseActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusUtils.register(BaseActivity.this);
    }

    protected void showAlert(String text) {
        SnackbarManager.show(
                Snackbar.with(getApplicationContext())
                        .text(text)
                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT),
                BaseActivity.this
        );
    }

}
