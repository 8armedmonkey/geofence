package com.eightam.android.geofence;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.eightam.android.geofence.constant.K;
import com.eightam.android.geofence.util.FormatterUtils;

public class GeofenceUpdateBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = GeofenceUpdateBroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "***** receive update, intent: " + FormatterUtils.intentAsString(intent));

        String action = intent.getAction();

        if (K.intent.action.ACTION_NOTIFY_GEOFENCE_TRANSITION.equals(action)) {
            Intent forwardIntent = new Intent(K.intent.action.ACTION_FORWARD_NOTIFY_GEOFENCE_TRANSITION);
            forwardIntent.putExtras(intent.getExtras());

            context.sendOrderedBroadcast(forwardIntent, null);
        }
    }

}
