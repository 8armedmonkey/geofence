package com.eightam.android.geofence;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.eightam.android.geofence.constant.K;
import com.eightam.android.geofence.manager.GeofenceManagerImpl;
import com.eightam.android.geofence.util.FormatterUtils;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

public class AppInBackgroundBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (K.intent.action.ACTION_FORWARD_NOTIFY_GEOFENCE_TRANSITION.equals(action)) {
            GeofencingEvent event = GeofencingEvent.fromIntent(intent);

            if (event != null && !event.hasError()) {
                showNotificationForGeofencingEvent(context, event);
            }
        }
    }

    private void showNotificationForGeofencingEvent(Context context, GeofencingEvent event) {
        GeofenceApplication application = (GeofenceApplication) context.getApplicationContext();
        GeofenceManagerImpl geofenceManager = (GeofenceManagerImpl) application.getGeofenceManager();

        List<Geofence> geofences = event.getTriggeringGeofences();
        int geofenceTransition = event.getGeofenceTransition();

        if (geofences != null) {
            for (Geofence geofence : geofences) {
                com.eightam.android.geofence.model.Geofence geofenceData = geofenceManager.getGeofence(geofence.getRequestId());

                if (geofenceData != null) {
                    NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
                    style.bigText(geofenceData.description);
                    style.setBigContentTitle(FormatterUtils.geofenceTransitionEvent(context, geofenceTransition, geofenceData));
                    style.setSummaryText(geofenceData.description);

                    Notification notification = new NotificationCompat.Builder(context.getApplicationContext())
                            .setAutoCancel(true)
                            .setStyle(style)
                            .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_LIGHTS)
                            .build();

                    NotificationManagerCompat.from(context.getApplicationContext()).notify(
                            geofenceData.id, K.notification.NOTIFICATION_ID_GEOFENCE_EVENT, notification);
                }
            }
        }
    }

}
