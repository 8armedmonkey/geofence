package com.eightam.android.geofence;

import android.app.Application;

import com.eightam.android.geofence.manager.GeofenceManagerImpl;
import com.eightam.android.geofence.manager.GoogleApiClientManager;
import com.eightam.android.geofence.manager.GeofenceManager;
import com.eightam.android.geofence.provider.GeofenceManagerProvider;
import com.google.android.gms.location.LocationServices;

public class GeofenceApplication extends Application
        implements GeofenceManagerProvider {

    private GoogleApiClientManager mGoogleApiClientManager;
    private GeofenceManager mGeofenceManager;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeGoogleClientApi();
        initializeGeofenceManager();
    }

    @Override
    public GeofenceManager getGeofenceManager() {
        return mGeofenceManager;
    }

    private void initializeGoogleClientApi() {
        mGoogleApiClientManager = new GoogleApiClientManager(getApplicationContext(), LocationServices.API);
        mGoogleApiClientManager.connect();
    }

    private void initializeGeofenceManager() {
        mGeofenceManager = new GeofenceManagerImpl(getApplicationContext(), mGoogleApiClientManager.getGoogleApiClient());
    }

}
