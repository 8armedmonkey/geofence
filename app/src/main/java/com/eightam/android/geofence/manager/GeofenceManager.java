package com.eightam.android.geofence.manager;

import com.eightam.android.geofence.model.Geofence;

import java.util.List;

public interface GeofenceManager {

    void addGeofence(Geofence geofence) throws Exception;

    void removeGeofence(String requestId) throws Exception;

    void clearGeofences() throws Exception;

    List<String> getGeofenceRequestIds();

    List<Geofence> getGeofences();

    Geofence getGeofence(String requestId);

}
