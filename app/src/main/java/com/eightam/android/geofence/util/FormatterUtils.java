package com.eightam.android.geofence.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.eightam.android.geofence.R;
import com.google.android.gms.location.Geofence;

import java.util.Iterator;

public class FormatterUtils {

    public static String geofenceTransitionEvent(
            Context context, int geofenceTransition, com.eightam.android.geofence.model.Geofence geofenceData) {
        int titleResourceId = R.string.msg_geofence_transition_event;

        switch (geofenceTransition) {
            case Geofence.GEOFENCE_TRANSITION_ENTER: {
                titleResourceId = R.string.msg_geofence_transition_enter;
                break;
            }

            case Geofence.GEOFENCE_TRANSITION_DWELL: {
                titleResourceId = R.string.msg_geofence_transition_dwell;
                break;
            }

            case Geofence.GEOFENCE_TRANSITION_EXIT: {
                titleResourceId = R.string.msg_geofence_transition_exit;
                break;
            }
        }

        return context.getResources().getString(titleResourceId, geofenceData.title);
    }

    public static String intentAsString(Intent intent) {
        StringBuilder sb = new StringBuilder();

        sb.append("Intent: { ");

        sb.append("action: ").append(intent.getAction());

        sb.append(", ");

        sb.append("extras: { ");

        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            Iterator<String> iterator = bundle.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                sb.append(key).append(": ").append(bundle.get(key));

                if (iterator.hasNext()) {
                    sb.append(", ");
                }
            }
        }

        sb.append(" }");

        sb.append(" }");

        return sb.toString();
    }

}
