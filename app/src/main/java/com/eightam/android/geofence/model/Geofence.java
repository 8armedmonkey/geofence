package com.eightam.android.geofence.model;

import com.google.android.gms.maps.model.LatLng;

public class Geofence {

    public String id;
    public String title;
    public String description;
    public LatLng coordinate;
    public float radius;

}
