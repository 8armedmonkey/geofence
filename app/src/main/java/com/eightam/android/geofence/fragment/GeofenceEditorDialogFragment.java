package com.eightam.android.geofence.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.eightam.android.geofence.R;
import com.eightam.android.geofence.model.Geofence;

import de.greenrobot.event.EventBus;

public class GeofenceEditorDialogFragment extends DialogFragment
        implements DialogInterface.OnClickListener {

    private Geofence mGeofence;

    private EditText mEditTitle;
    private EditText mEditDescription;
    private EditText mEditRadius;

    public static GeofenceEditorDialogFragment newInstance(Geofence geofence) {
        GeofenceEditorDialogFragment fragment = new GeofenceEditorDialogFragment();
        fragment.mGeofence = geofence;

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();

        View view = LayoutInflater.from(context).inflate(R.layout.fragment_geofence_editor, null);

        mEditTitle = (EditText) view.findViewById(R.id.edit_title);
        mEditDescription = (EditText) view.findViewById(R.id.edit_description);
        mEditRadius = (EditText) view.findViewById(R.id.edit_radius);

        mEditTitle.setText(mGeofence.title);
        mEditDescription.setText(mGeofence.description);
        mEditRadius.setText(String.format("%.02f", mGeofence.radius));

        return new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton(R.string.button_save, GeofenceEditorDialogFragment.this)
                .setNegativeButton(R.string.button_cancel, null)
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE: {
                saveGeofence();
                dismiss();
                break;
            }
        }
    }

    private void saveGeofence() {
        mGeofence.title = mEditTitle.getText().toString();
        mGeofence.description = mEditDescription.getText().toString();

        try {
            mGeofence.radius = Float.parseFloat(mEditRadius.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        raiseOnGeofenceUpdatedEvent();
    }

    private void raiseOnGeofenceUpdatedEvent() {
        OnGeofenceUpdatedEvent event = new OnGeofenceUpdatedEvent();
        event.geofence = mGeofence;

        EventBus.getDefault().post(event);
    }

    public static class OnGeofenceUpdatedEvent {

        public Geofence geofence;

    }

}
